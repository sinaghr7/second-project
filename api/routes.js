
var router = require("express").Router();
router.use('/users',require('./users/router'))
router.use('/services', require('./services/router'))
router.use('/user-services',require('./userServices/router'))

module.exports = router;