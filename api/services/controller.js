const Service = require('../../model/service')

// Retrieve all Services from the database.
exports.findAll = (req, res) => {
    Service.find()
    .then(data => {
        res.send(data);
    })
    .catch(err => {
        res.status(500).send({
            message:
                err.message || "Some error occurred while retrieving services."
        });
    });
};

// Find single Service 
exports.findOne = (req, res) => {
    let id = req.params.id;
    Service.findById(id)
        .then(data => {
            if (!data)
                res.status(404).send({message: "Not found service with id " + id});
            else res.send(data);
        })
        .catch(err => {
            res
                .status(500)
                .send({message: "there is no service with id =" + id});
        });
};

// Create and Save a new Service 
exports.create = (req, res) => {
  // Validate request
  if (!req.body.title) {
    res.status(400).send({message: "title can not be empty!"});
    return;
}
Service.create({
    title: req.body.title,
    description: req.body.description,
    orders: req.body.orders,
    price: req.body.price
})
    .then(data => {
        res.send(data);
    })
    .catch(err => {
        if(err.code === 11000){
            return (res.status(400).send({
                message:
                 "title in already in use"
            }));
        }
        res.status(500).send({
            message:
                err.message || "Some error occurred while creating the service."
        });
    });
};

// Update Service 
exports.update = (req, res) => {
    if (!req.body) {
        return res.status(400).send({
            message: "Data to update can not be empty!"
        });
    }
    const id = req.params.id;
    if(req.user._id === id || req.user.role === "admin" ){
    Service.findByIdAndUpdate(id, req.body, { useFindAndModify: false })
        .then(data => {
            if (!data) {
                res.status(404).send({
                    message: `Cannot update Service with id=${id}. Maybe Service was not found!`
                });
            } else res.send({ message: "service was updated successfully." });
        })
        .catch(err => {
            res.status(500).send({
                message: "Error updating service(maybe title is already exist"
            });
        });
    }else 
    {
        res
            .status(401)
            .send({message: "You Dont Have Access"});
    }
};

// Delete Service 
exports.deleteOne = (req, res) => {
    let id = req.params.id;
    if(req.user._id === id || req.user.role === "admin" ){
    Service.findByIdAndRemove(id)
      .then(data => {
        if (!data) {
          res.status(404).send({
            message: "Cannot delete Service. Maybe Service was not found!"
          });
        } else {
          res.send({
            message: "Service was deleted successfully!"
          });
        }
      })
      .catch(err => {
        res.status(500).send({
          message: "Could not delete Service with id=" + id
        });
      });
    }else 
    {
        res
            .status(401)
            .send({message: "You Dont Have Access"});
    }
};