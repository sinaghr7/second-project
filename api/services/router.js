const {validate ,role} = require ("../../middlewares/auth");
const {findAll,findOne,create,update,deleteOne} = require("./controller");
var router = require("express").Router();

router.route('/')
    // Retrieve all Services
    .get(validate , findAll)

    // Create a new service
    .post(create);

    router.route('/admin/:id')
    // Retrieve single Service 
    .get(validate ,findOne)

    // Update Service(admin)
    .put(validate ,role('admin'),update)

    // Delete Service(admin)
    .delete(validate ,role('admin'),deleteOne);

    router.route('/:id')
    // Update Service(user)
    .put(validate,update)

    // Delete Service(user)
    .delete(validate,deleteOne)

    // Retrieve single Service 
    .get(validate ,findOne);

module.exports = router;