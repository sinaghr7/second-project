const UserService = require('../../model/userServices')

// Retrieve all UserServices from the database.
exports.findAll = (req,res) => {
    UserService.find()
    .populate('userId','username firstName lastName')
    .populate('serviceId' ,'title description').exec()
    .then(data => {
        res.send(data);
    })
    .catch(err => {
        res.status(500).send({
            message:
                err.message || "Some error occurred while retrieving  UserService."
        });
    });
};

// Find single UserService
exports.findOne = (req, res) => {
    let id = req.params.id;
    UserService.findById(id)
    .populate('userId','username firstName lastName')
    .populate('serviceId' ,'title description').exec()
        .then(data => {
            if (!data)
                res.status(404).send({message: "Not found  UserService"});
            else res.send(data);
        })
        .catch(err => {
            res
                .status(500)
                .send({message: "there is no  UserService with id =" + id});
        });
};

// Create and Save a new UserService
exports.create = (req, res) => {
   // Validate request
   if (!req.body.userId) {
    res.status(400).send({message: "userId can not be empty!"});
    return;
}
UserService.create({
    userId: req.body.userId,
    serviceId: req.body.serviceId,
    status: req.body.status,
    start: req.body.start,
    finish: req.body.finish,
})
    .then(data => {
        res.send(data);
    })
    .catch(err => {
        res.status(500).send({
            message:
                err.message || "Some error occurred while creating the UserService."
        });
    });
};

// Update UserService
exports.update = (req, res) => {
    if (!req.body) {
        return res.status(400).send({
            message: "Data to update can not be empty!"
        });
    }
    const id = req.params.id;
    UserService.findByIdAndUpdate(id, req.body, { useFindAndModify: false })
        .then(data => {
            if (!data) {
                res.status(404).send({
                    message: `Cannot update  UserService. Maybe  UserService was not found!`
                });
            } else res.send({ message: " UserService was updated successfully." });
        })
        .catch(err => {
            res.status(500).send({
                message: "Error updating  UserService"
            });
        });
};

// Delete UserService
exports.delete = (req, res) => {
    let id = req.params.id;
    UserService.findByIdAndRemove(id)
      .then(data => {
        if (!data) {
          res.status(404).send({
            message: "Cannot delete UserService. Maybe  UserService was not found!"
          });
        } else {
          res.send({
            message: " UserService was deleted successfully!"
          });
        }
      })
      .catch(err => {
        res.status(500).send({
          message: "Could not delete  UserService"
        });
      });
};