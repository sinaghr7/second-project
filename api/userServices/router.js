const UserServices = require("./controller");
var router = require("express").Router();

    router.route('/')
    // Retrieve all UserServices
    .get(UserServices.findAll)

    // Create new UserService
    .post(UserServices.create);

    router.route('/:id')
    // Retrieve single UserService 
    .get(UserServices.findOne)

    // Update UserService
    .put(UserServices.update)

    // Delete UserService
    .delete(UserServices.delete);

module.exports = router;
