const User = require('../../model/user')
const authMiddleware = require('../../middlewares/auth')
const bcrypt = require('bcrypt')
const jwt = require('jsonwebtoken')
// Retrieve all Users from the database.
exports.findAll = (req, res) => {
    User.find()
        .then(data => {
            res.send(data);
        })
        .catch(err => {
            res.status(500).send({
                message:
                    err.message || "Some error occurred while retrieving users."
            });
        });
};

// Find single User 
exports.findOne = (req, res) => {
    let id = req.params.id;
    User.findById(id)
        .then(data => {
            if (!data)
                res.status(404).send({message: "Not found User with id " + id});
            else res.send(data);
        })
        .catch(err => {
            res
                .status(500)
                .send({message: "there is no user with id =" + id});
        });
};

// Sign up
exports.create = (req, res) => {
    // Validate request
    if (!req.body.username) {
        res.status(400).send({message: "username can not be empty!"});
        return;
    }
    User.create({
        username: req.body.username,
        password: req.body.password,
        firstName: req.body.firstName,
        lastName: req.body.lastName,
        role: 'user'
    })
        .then(async data => {
            let token = await authMiddleware.jwt(data._id);
            res.send(token);
        })
        .catch(err => {
            if (err.code === 11000) {
                return (res.status(406).send({
                    message:
                        "username in already in user"
                }));
            }
            res.status(500).send({
                message:
                    err.message || "Some error occurred while creating the user."
            });
        });

};

//Sign in
exports.Login = (req, res) => {
    // Validate request
    if (!req.body.username) {
        res.status(400).send({message: "username can not be empty!"});
        return;
    }
    User.findOne({username: req.body.username})
        .then(async data => {
            if (!data) {
                return res.status(404).send('Incorrect username.');
            }
            const validPassword = await bcrypt.compare(req.body.password, data.password);
            if (!validPassword) {
                return res.status(400).send('Incorrect password.');
            } else {
                let token = await authMiddleware.jwt(data._id);
                res.send(token);
            }
        })
        .catch(err => {
            res.status(500).send({
                message:
                    err.message || "Some error occurred while Sign In."
            });
        });
};

// Update User
exports.update = (req, res) => {
    if (!req.body) {
        return res.status(400).send({
            message: "Data to update can not be empty!"
        });
    }
    const id = req.params.id;
    if (req.user._id === id || req.user.role === "admin") {
        User.findByIdAndUpdate(id, req.body, {useFindAndModify: false})
            .then(data => {
                if (!data) {
                    res.status(404).send({
                        message: `Cannot update user with id=${id}. Maybe user was not found!`
                    });
                } else res.send({message: "user was updated successfully."});
            })
            .catch(err => {
                res.status(500).send({
                    message: "Error updating user(maybe username is already exist)"
                });
            });
    } else {
        res
            .status(401)
            .send({message: "You Dont Have Access"});
    }
};

// Delete User
exports.deleteOne = (req, res) => {
    let id = req.params.id;
    if (req.user._id === id || req.user.role === "admin") {
        User.findByIdAndRemove(id)
            .then(data => {
                if (!data) {
                    res.status(404).send({
                        message: "Cannot delete User with id=${id}. Maybe User was not found!"
                    });
                } else {
                    res.send({
                        message: "User was deleted successfully!"
                    });
                }
            })
            .catch(err => {
                res.status(500).send({
                    message: "Could not delete User with id=" + id
                });
            });
    } else {
        res
            .status(401)
            .send({message: "You Dont Have Access"});
    }
};