const {findAll,findOne,create,Login,update,deleteOne} = require("./controller");
const {validate ,role} = require ("../../middlewares/auth");
var router = require("express").Router();

    router.route('/')
    // Retrieve all Users
    .get(validate , findAll)

    // Sign up
    .post(create);

    router.route('/login')
    // Sign in
    .post(Login);

    router.route('/admin/:id')
    // Retrieve single User 
    .get(validate ,findOne)

    // Update User(admin)
    .put(validate ,role('admin'),update)

    // Delete User(admin)
    .delete(validate ,role('admin'),deleteOne);

    router.route('/:id')
    // Update User(user)
    .put(validate,update)

    // Delete User(user)
    .delete(validate,deleteOne)

    // Retrieve single User 
    .get(validate ,findOne);


module.exports = router;
