const jwt = require('jsonwebtoken');
const User = require('../model/user');

//jwt token 
exports.jwt = async (Userid) => {
    return new Promise((resolve) => {
        const payload = {
            User: {
                _id: Userid
            }
        };
        jwt.sign(
            payload,
            'secret key',
            {expiresIn: 604800},
            function (err, token) {
                if (err) throw err;
                resolve(token);
            }
        );
    })
};

//token validation

exports.validate = (req, res, next) => {
    var bearer = req.headers.authorization.replace('Bearer ', '');
    const decode = jwt.verify(bearer, 'secret key');
    console.log(decode);
    const id = decode.User._id;
    User.findById(id)
        .then(user => {
            console.log(user);
            req.user = user;
            next();
        })
        .catch(err => {
            res
                .status(401)
                .send({message: "invalid token"});
        });
};

//check role

exports.role = (role) => {
    return function (req, res, next) {
        if (role === req.user.role)
            next();
        else
            res
                .status(401)
                .send({message: "Ops!! Just Admin"});
    }
};
