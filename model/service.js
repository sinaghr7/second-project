const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let ServiceSchema = new Schema({
    title: {type: String, required: true, max: 50 ,index: true , unique: true},
    description: {type: String, required: true},
    orders: {type: String, required: false},
    price: {type: Number, required: false},
});

// Export the model
module.exports = mongoose.model('Service', ServiceSchema);