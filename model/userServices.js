const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let userServicesSchema = new Schema({
    userId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User'
      },
      serviceId: {
        type: mongoose.ObjectId,
        ref: 'Service'
      },
    status: {type: Number, required: false},
    start: {type: Number, required: false}, 
    finish: {type: Number, required: false},
});

// Export the model
module.exports = mongoose.model('userServices', userServicesSchema);