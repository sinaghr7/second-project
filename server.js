const express = require('express')
const mongoose = require('mongoose')
const app = express();
app.use(express.json());

//DB Connection
mongoose.connect('mongodb://localhost:27017/prj2', {
    useNewUrlParser: true,
    useUnifiedTopology: true
})
    .then(() => console.log('connected successfully'))
    .catch((err) => console.error(err))
//DB Connection End

// routes
var routes = require('./api/routes');
app.use('/',routes);

app.listen(3000, function () {
    console.log('Server is listening at 3000')
});
